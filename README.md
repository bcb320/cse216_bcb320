# CSE 216
This is a java-backend based web application I wrote for a potential web forum, with a respective Android app all built from scratch.
I used Docker to host a SQL database, which is used in backend to serve data to both web and android apps using REST API.

This served as the backbone for a semester long, team-based project in which users could sign in using Google login, vote for messages and comments, 
upload photos and pdfs (using cloud hosting services in backend). 
This semester-long project was hosted on Heroku, and was only available for log in by Lehigh University staff and students (a lehigh.edu email was needed for sign in).  


## Details
- Semester: Fall 2018
- Student ID: bcb320
- Bitbucket Repository: https://bcb320@bitbucket.org/bcb320/cse216_bcb320.git

## Contributors
1. Briana Brady